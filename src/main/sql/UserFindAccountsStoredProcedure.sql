drop procedure m.findAllAccountsByClientId;
GO;
create procedure m.findAllAccountsByClientId
    @clientId bigint
as
    select
         a.clientId          as user_id
        ,a.id                as id
        ,a.number            as number
        ,a.balance           as balance
        ,c.isoCode           as currency_iso
        ,hab.createdDate     as last_transaction
      from m.account         as a
      inner join m.currency  as c on c.id = a.currencyId
      outer apply (select top 1
                        h.createdDate
                     from m.historyAccountBalance as h
                     where h.accountId = a.id
                     order by h.createdDate desc)
                             as hab
      where a.clientId = @clientId;
GO;

execute m.findAllAccountsByClientId 2;
