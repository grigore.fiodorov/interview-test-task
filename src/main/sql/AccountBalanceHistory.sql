drop table if exists m.historyAccountBalance;

create table m.historyAccountBalance (
    id          bigint identity(1,1) not null,
    accountId   tinyint              not null,
    createdDate datetime             not null default CURRENT_TIMESTAMP,
    amount      numeric(8,2)         not null,
    constraint pk_history_account_balance            primary key (id),
    constraint fk_history_account_balance_account_id foreign key (accountId) references m.account(id)
);

insert into m.historyAccountBalance (accountId, amount)
values (1, 200);

insert into m.historyAccountBalance (accountId, amount)
values (2, 400);

select * from m.historyAccountBalance;