drop table if exists m.client;

create table m.client (
    id        bigint identity(1,1) not null,
    lastName  varchar(100)         not null,
    firstName varchar(100)         not null,
    constraint pk_client_id primary key (id)
);

insert into m.client (lastName, firstName)
values ('test','test');

insert into m.client (lastName, firstName)
values ('test2', 'test2');