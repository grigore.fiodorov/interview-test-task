drop table if exists m.currency;

create table m.currency (
    id      tinyint identity(1,1) not null,
    isoCode varchar(3)            not null,
    constraint pk_currency_id primary key (id)
);

insert into m.currency (isoCode)
values ('EUR');
