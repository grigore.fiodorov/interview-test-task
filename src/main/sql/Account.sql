drop table if exists m.account;

create table m.account (
    id tinyint identity(1,1) not null,
    number     varchar(100)  not null,
    clientId   bigint        not null,
    currencyId tinyint       not null,
    balance numeric(8,2)     not null,
    constraint pk_account_id primary key (id),
    constraint fk_account_client_id foreign key (clientId) references m.client(id),
    constraint fk_account_currency_id foreign key (currencyId) references m.currency(id)
);

insert into m.account (number, clientId, currencyId, balance)
values ('2t24fg23f23', 1, 1, 10000);

insert into m.account (number, clientId, currencyId, balance)
values ('34f4f34f34f34', 2, 1, 20000);

select * from m.account