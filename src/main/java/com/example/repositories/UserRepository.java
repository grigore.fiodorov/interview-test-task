package com.example.repositories;

import com.example.models.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface UserRepository extends org.springframework.data.repository.Repository<Account, Long> {
    @Query(value = "execute m.findAllAccountsByClientId :userId", nativeQuery = true)
    List<Account> findAccountsByUserId(@Param("userId") Long userId);
}
