package com.example.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Column;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Account {

    @Id
    private Long id;

    @Column("user_id")
    private long userId;

    private String number;

    private double balance;

    @Column("currency_iso")
    private String currencyIso;

    @Column("last_transaction")
    private String lastTransaction;

}
